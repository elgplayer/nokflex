# NOKFLEX PYTHON

A library that gets data from NOKFLEX and serves it localy

### Requirments

To install, make sure you have python 3.6 or newer
Then: `pip install -r requirements.txt`

### HOW-TO

1. Paste your token in the `deb/token.txt`

2. Run the `src/main.py`

### The scripts

* main.py : Main program

License
----

MIT


