# -*- coding: utf-8 -*-
'''
Utils that handles things
'''

import os
import sys
import signal
import logging
import datetime
import json


# Logger init
logger = logging.getLogger(__name__)


def create_logger():
    '''
    Creates a logger to log the script
    
    returns: logger object
    '''
    
    date_today = datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S')
    log_file_name = "../log/{}.log".format(date_today)

    # Create logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    
    # Create file handler and set level to debug
    fh = logging.FileHandler(log_file_name, mode = 'w')
    fh.setLevel(logging.DEBUG)
    
    # Create console handler with a higher log level
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.INFO)
    
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)8s --- %(message)s ' +
                                  '(%(filename)s:%(lineno)s)', 
                                  datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # Add the handlers to the logger
    root_logger.addHandler(fh)
    root_logger.addHandler(ch)
    
    return logger


def keyboardInterruptHandler(signal, frame):
    '''
    Stops the program if a keyboard intertupt is detected (CTRL + C)

    Attributes:
        * signal (signal object): What signal it will be detecting
        * frame (???): ???
    '''

    print("\nKeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
    logging.info("Stopping program")

    # Exits
    exit(0)


def create_structure():
    '''
    Creates the proper directory structure
    '''

    folders = ['deb', 'log', 'output', 'output/5c/html', 'output/5c/scraped_data']
    base_path = '../'

    # Goes through the folders
    for folder in folders:

        # Concatenates the folders
        folder_path = '{}/{}'.format(base_path, folder)

        # Check if the path exists
        if not os.path.exists(folder_path):

            # Creates the directory
            os.makedirs(folder_path, exist_ok=True)
            logging.debug("Created directory: {}".format(folder_path))


def check_if_exists(file_path):
    '''
    Check if a file exists

    Attributes:
        * file_path (str): Path to check if exists

    return: String 
    '''

    if not os.path.exists(file_path):

        return 'NOT_EXISTS'

    return 'EXISTS'


def write_json(data, file_path, logger_msg="Data written to disk"):
    '''
    Writes JSON data to disk

    Attributes:
        * data (ANY): Data that will be written to file
        * file_path (str): Path where to store the data
        * logger_msg (str): Logger debug message
    '''

    # Open the file
    with open(file_path, 'w', encoding='utf-8') as outfile:
        
        # Writes the file as JSON
        json.dump(data, outfile, indent=4, 
                  ensure_ascii=False)
        logging.debug(logger_msg)


def read_json(file_path, logger_msg=""):
    '''
    Reads a JSON file and either returns the file contents or returns an error

    Attributes:
        * file_path (str): Path where to store the data
        * logger_msg (str): Logger debug message

    return: JSON data (dict) or error code (str)
    '''

    # Sets logger message
    if logger_msg == "":
        logger_msg = "Reading file: {}".format(file_path)

    export_dict = {'status': 0}

    if not os.path.exists(file_path):

        logger.debug("File does not exists: {}".format(file_path))
        export_dict['status'] = 'FILE_NOT_EXISTS'
        
        return export_dict

    try:

        # Open the file from disk
        with open(file_path, encoding='utf-8') as json_file:

            json_data = json.load(json_file, encoding='utf-8')
            export_dict = {'status': 1, 'data': json_data}
            logger.debug(logger_msg)       

    except Exception as e:

        logging.info("Solution file error: {}".format(e))
        export_dict['status'] = 'JSON_FILE_ERROR'

        pass


    return export_dict


def write_token(state_dict):
    '''
    Writes the token to disk

    Attributes:
        * state_dict (dict): Dictionary with the programs current state
    '''

    # File path
    file_path = '../deb/token.txt'

    # Open the file
    with open(file_path, 'w') as f:

        # Read the file data
        f.write(state_dict['token'])
        logger.debug("Token written to file") 


def ask_for_token(state_dict):
    '''
    Asks for token

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: State dict    
    '''

    # Get token input
    token = input("Input a valid token: ")
    logger.debug("Asking for token")
    
    # Sets the state_dict variables
    state_dict['token'] = token
    state_dict['token_valid'] = 1

    # Write to file
    write_token(state_dict)
    
    return state_dict


def read_token(state_dict):
    '''
    Read token and sets the value in the state dict

    Attributes:
        * state_dict (dict): Dictionary with the programs current state
        
    return: State dict
    '''

    # File path
    file_path = '../deb/token.txt'

    # Check if file exists
    

    if check_if_exists(file_path) == 'NOT_EXISTS':

        logger.info("Token file does not exists")
        state_dict = ask_for_token(state_dict)

    # Open the file
    with open(file_path, 'r') as f:

        # Read the file data
        data = f.read()
        logger.debug("Token read from file") 

        # Check that the token it atleast valid
        if 'Bearer' not in data:

            state_dict = ask_for_token(state_dict)

        else:

            state_dict['token'] = data


    logger.debug("Token loaded: {}".format(state_dict['token']))
            
    
    return state_dict


signal.signal(signal.SIGINT, keyboardInterruptHandler)