# -*- coding: utf-8 -*-
'''
Generates all HTML code for export
'''

import webbrowser
import os
import logging
import platform

import utils

# Get the loggers name
logger = logging.getLogger(__name__)


def parse_math_tag(str_input):
    '''
    Replaces invalid MathML scheme tags with a just a math tag

    Attributes:
        * str_input (str): Text to convert

    return: Parsed string
    '''     
    
    str_input = str_input.replace(
            '<math xmlns="http://www.w3.org/1998/Math/MathML">', '<math>')
    str_input = str_input.replace('<section>', '')
    str_input = str_input.replace('</section>', '')
    
    return str_input


def parse_linebreak(str_input):
    '''
    Replaces invalid newspaces tags with valid ones

    Attributes:
        * str_input (str): Text to convert

    return: Parsed string
    '''        
    str_input = str_input.replace('<mspace linebreak="newline">', 
                                  "<mspace linebreak='newline' />")
    
    return str_input
    

def remove_div(str_input):
    '''
    Removes div tags from math XML

    Attributes:
        * str_input (str): Text to convert

    return: Parsed string
    '''    

    str_input = str_input.replace('<div>', '')
    str_input = str_input.replace('</div>', '')
    
    return str_input


def mathml_converter(selected_text):
    '''
    Creates a HTML header given text and size

    Attributes:
        * selected_text (str): Text to convert

    return: Parsed and cleaned up XML
    '''
    
    new_text = selected_text
    new_text = parse_math_tag(new_text)
    new_text = parse_linebreak(new_text)
    new_text = remove_div(new_text)
    new_text += "\n"
    

    return new_text


def create_header(header_text, header_size):
    '''
    Creates a HTML header given text and size

    Attributes:
        * header_text (str or num): Text to set as header
        * header_size (int): How large the text should be 
                                [1 largest --> 5 smallest]    
    '''
    
    header_html = '<h{}> {} </h{}>'.format(header_size, header_text, header_size)
    
    return header_html


def html_boilerplate():
    '''
    Returns the base of the HTML boilerplate

    return: HTML boilerplate (str)
    '''

    base_html = '''\
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript"
            src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        </script>
    </head>
    <body>
    '''
    
    return base_html


def index_boilerplate():
    '''
    Returns the base of the index boilerplate

    return: HTML boilerplate (str)
    '''

    base_html = '''\
<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/main.css">
</head>
<body>
'''
    return base_html 


def create_link(link_path, link_name, file=1):
    '''
     Creates HTML links

    Attributes:
        * link_path (str): Where the file is located
        * link_name (str): What the link should display
        * file (str): Boolean value if the link points to a file or not

    return: Link (str)   
    '''

    if file == 1:

        link_path = 'file://' + link_path

    link = '<a href="{}">{}</a><br>'.format(link_path, link_name)

    return link


def html_boilerplate_end():
    '''
    Returns the closing tags to the base_html

    return: HTML boilerplate (str)
    '''
    base_html_end =  '''\
    </body>
</html>
'''
    return base_html_end



def write_to_file(file_path, html, 
                  logger_msg="HTML written to file"):
    '''
    Writes the HTML to an file

    Attributes:
        * file_path (str): Where the file is located
        * html (str): HTML string that will be written to the file
        * logger_msg (str): What should be written to the log
    '''

    # Writes to file
    with open(file_path, 'w') as f:

        f.write(html)
        logger.debug(logger_msg)


def generate_html_path(state_dict):


    assignment_id = state_dict['assignment_id']
    output_folder = '../output/5c'
    index_file_path = os.path.abspath(output_folder) + "/index.html"
    file_path = "{}/html/{}.html".format(output_folder, assignment_id)
    file_path_abs = os.path.abspath(output_folder) + "/html/{}.html".format(assignment_id)    

    path_dict = {'file_path_abs': file_path_abs, 
                 'index_file_path': index_file_path,
                 'file_path': file_path}

    return path_dict


def generate_index():
    '''
    Generates a HTML index to view all assignment in
    '''

    index_folder = '../output/5c'
    index_file_path = "../output/5c/index.html"
    html_folder = '{}/html'.format(index_folder)
    absolute_path = os.path.abspath(html_folder)

    # Get all files in the directory
    files = os.listdir(html_folder)

    # Base HTML
    index_html = index_boilerplate()

    # Creat the header
    index_html += create_header("Task ID index for {}".format("5C"), 1)

    # Go through the file name
    for file_name in files:

        link_text = file_name

        # Concatenates a filepath
        file_path = '{}/{}'.format(absolute_path, file_name)

        # Appends the file to the index
        index_html += create_link(file_path, link_text, 1)
        
    # Add the last HTML to the index
    index_html += html_boilerplate_end()

    # Write to file
    write_to_file(index_file_path, index_html, "Index HTML written to file")


def generate_solution(state_dict):
    '''
    Generates an HTML site which parses the MathML for a readble solution

    Attributes:
        * state_dict (dict): Dictionary with the programs current state
    '''

    # Abbrevations    
    assignment_id = state_dict['assignment_id']
    solutions_dict = state_dict['solution']['solution']
    
    # Check if the solution is empty or not
    if solutions_dict == []:
        
        logging.info("Empty solution, returning...")
        return
        
    task_id = state_dict['solution']['solution'][0]['task']
    path_dict = generate_html_path(state_dict)
    
    # Init the HTML
    html = html_boilerplate()
    
    # Add the Link back to Index and Task ID header
    html += create_link(path_dict['index_file_path'], "Back to index", 1)
    html += create_header("TaskId: {} AssingmentId: {}".format(str(task_id), 
                          str(assignment_id)), 1)

    # Goes through the diffrent "subtasks"
    for kx,ky in enumerate(solutions_dict):
        
        selected_task = solutions_dict[kx]

        # Simple abbrevation
        sub_task = selected_task['subTask']
        assignment_id = selected_task['id']
        hints = selected_task['hints']
        answers = selected_task['answers']
        solutions = selected_task['solutions']
        
        # Check for subtask
        if sub_task != '':
            
            # Add subtask info
            sub_task_text = "{}".format(sub_task)
            html += create_header(sub_task_text, 1)
        
        # Hints
        html += create_header("Hints", 2)
        html += mathml_converter(hints)
        
        # Answers
        if state_dict['solution']['hasAnswer'] == True:

            html += create_header("Answers", 2)
            html += mathml_converter(answers)
        
        # Solutions
        if state_dict['solution']['hasSolution'] == True:
            
            html += create_header("Solutions", 2)
            html += mathml_converter(solutions)
        
        # Check if it's the last key
        if kx == (len(solutions_dict) - 1):
            
            # Add the last end tags for the HTML
            html += html_boilerplate_end()
            
            # Write the solution to file
            write_to_file(path_dict['file_path'], html, "Solution HTML generated")


def open_webbroswer(url):
    '''
    Opens the newly generated file in a new tab

    Attributes:
        * url (str): Web address to open
    '''
    
    # One method
    # webbrowser.get('windows-default').open('http://www.google.com')
    # webbrowser.get('macosx').open('http://www.google.com')

    # Open the webbrowser
    logging.info("Opening webbrowser to URL: {}".format(url))

    if platform.system() == "Darwin":
    	chrome_path = 'open -a /Applications/Google\ Chrome.app %s'
    	webbrowser.get(chrome_path).open(url)
    else:
    	webbrowser.get().open(url)


def solution_to_html(state_dict):
    '''
    Opens the solution in the webbrowser
    If the solution does not already exists, it generates an HTML page of it
    And generates an index

    '''

    # Path dict
    path_dict = generate_html_path(state_dict)

    if utils.check_if_exists(path_dict['file_path_abs']) == 'NOT_EXISTS':

        # Generate HTML solution if not
        generate_solution(state_dict)

        # Generate the index for each entry
        if (state_dict['mode'] == 'interactive_mode'):
        
            # Generate dict
            generate_index()

    if (state_dict['mode'] == 'interactive_mode'):

        # Open the browser
        open_webbroswer(path_dict['file_path_abs'])



