# -*- coding: utf-8 -*-
'''
Has all the program state info, so if a assignment already has been downloaded,
it keeps track on it
'''

import json
import logging
import sys

import utils
from functions import program_state
from functions import scraper
from functions import parser
from functions import html_gen
from functions import random_generators

# Get the loggers name
logger = logging.getLogger(__name__)


def init():
    '''
    Init the program

    return: state_dict
    '''

    # Create the sturcture
    utils.create_structure()

    # Init state_dict
    state_dict = {'token_valid': 1, 'assignment_id_valid': 0,
                  'token': '', 'assignment_id': '', 
                  'solution_file': {}, 'solution': None, 'error': 0,
                  'status': {}}

    # Reads the token
    state_dict = utils.read_token(state_dict)

    # Reads the solution
    state_dict = read_solution_file(state_dict)


    return state_dict


def read_solution_file(state_dict):
    '''
    Reads solution from file

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''

    # File path
    file_path = '../output/5c/scraped_data/solution.json'

    # Reads the data from file
    solution_data = utils.read_json(file_path, "Solution file loaded")

    if (solution_data['status']) == 1:

        state_dict['solution_file'] = solution_data['data']
        state_dict = parser.create_task_id_list(state_dict)

    else:

        state_dict['solution_file'] = {}

    return state_dict


def write_solution(state_dict):
    '''
    Writes the solution to file

    Attributes:
        * state_dict (dict): Dictionary with the programs current state
    '''

    # Check if the file exists
    

    # File path
    file_path = '../deb/solution.json'

    state_dict['solution_file'][state_dict['assignment_id']] = state_dict['solution']

    utils.write_json(state_dict['solution_file'], file_path, 
                     "Solution was written to file")


def ask_for_assignment_id(state_dict):
    '''
    Asks for assingment_id

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''

    logger.debug("Asking for assignment_id")
    input_str = input("Input assignment_id: ")
    assignment_id_valid = 0
    input_type = ""
    input_valid = 0

    # Maybe TaskId
    if len(input_str) == 4:

        # Do something
        try:

            valid_input_str = str(int(input_str))
            input_valid = 1
            input_type = "task_id"

        except:

            pass

    # Maybe Assignment Id
    if len(input_str) == 5:

        # Do something
        try:

            valid_input_str = str(int(input_str))
            input_valid = 1
            input_type = "assignment_id"

        except:

            pass

    # URL
    if 'https://nokflex.nok.se' in input_str:

        url_split = input_str.split('/')

        if len(url_split[-1]) == 5:

            try:

                valid_input_str = str(int(url_split[-1]))
                input_valid = 1
                input_type = "URL"

            except:

                pass

    # Check task_id
    if input_type == "task_id":

        valid_input_str = state_dict['task_id_translation'][valid_input_str]

    if input_str == "exit":

        # Exit the program
        logging.info("Exiting..")
        sys.exit(1)

    # Check if the input is valid
    if input_valid == 0:

        state_dict = ask_for_assignment_id(state_dict)


    logging.debug("Input Type: {} | Input string = {}".format(input_type, 
                                                              valid_input_str))
    state_dict['assignment_id'] = valid_input_str
    state_dict['assignment_id_valid'] = input_valid

    logger.debug("Getting assignment_id: {}".format(valid_input_str))
    
    
    return state_dict


def check_for_solution(state_dict):
    '''
    Check if the assignment Id already exists in the solution file

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: Dictionary with boolean value if it exists and state_dict
    '''

    downloaded = 0
    state_dict['assignment_id'] = str(state_dict['assignment_id'])

    # Check if it exists
    if state_dict['assignment_id'] in state_dict['solution_file']:

        # Sets the exisisting solution as the current solution
        downloaded = 1
        state_dict['solution'] = state_dict['solution_file'][state_dict['assignment_id']]
        state_dict['exists'] = 1

        logger.info("Assignment already downloaded")

    state_dict['status']['downloaded'] = downloaded


    return state_dict


def interactive_mode(state_dict):
    '''
    Starts interactive mode

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: State_dict
    '''

    loop_counter = 0

    # Main program loop
    while True:

        # Loop counter
        logging.debug("Loop counter: {}".format(loop_counter))
        loop_counter += 1
        
        # If the token is invalid
        if state_dict['token_valid'] == 0:

            state_dict = program_state.ask_for_token(state_dict)

        # If the assignment has been completed
        if state_dict['assignment_id_valid'] == 0:

            state_dict = program_state.ask_for_assignment_id(state_dict)

        # If both are new --> Get the solution
        if state_dict['token_valid'] == 1 and state_dict['assignment_id_valid'] == 1:

            # Gets the solutions
            state_dict = scraper.get_solution(state_dict)

            # Process the solution
            state_dict = parser.process_solution(state_dict)


    return state_dict


def scrape_mode(state_dict):
    '''
    Scrapes all data from nokflex

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: Dictionary with boolean value if it exists and state_dict
    '''    

    state_dict['chapters'] = {}
    state_dict['chapter_id'] = ""
    list_file_path = '../output/5c/scraped_data/assignments_list.json'

    # Reads the assignment list
    assignments_list = utils.read_json(list_file_path, "assignmentsId_list")

    # Check if the assignment already exists
    if assignments_list['status'] == 1:

        logger.info("Assignment list already exists")
        state_dict['assignments_list'] = assignments_list['data']

    else:

        # Downloads all assignments
        logger.info("Scraping assignments")

        # Get the hierarchy
        state_dict = scraper.get_hierarchy(state_dict)

        # Download the assignments
        state_dict = scraper.download_assignments(state_dict)

        state_dict['assignments_list'] = assignments_list


    # Goes through the assignments
    for k,x in enumerate(state_dict['assignments_list']):

        state_dict['assignment_id'] = state_dict['assignments_list'][k]
        
        logging.info("Assignment: {} | {}/{}".format(state_dict['assignment_id'], k, 
                                                     len(state_dict['assignments_list']) -1))
        
        # Gets the solution for a assignment
        state_dict = scraper.get_solution(state_dict)

        # Process the solution
        state_dict = parser.process_solution(state_dict)

    # Generate the index
    html_gen.generate_index()


    return state_dict


def login_mode(state_dict):
    '''
    Logins to nokflex until X number of sign-ins to get login achivments
    
    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''

    # Gets the current login count
    state_dict = scraper.login(state_dict)

    # Gets the 1000 logins achivment
    while state_dict['login_count'] < 1000:

        # Check if the token is valid
        if state_dict['token_valid'] == 0:
        
            # Get the token
            state_dict = utils.ask_for_token(state_dict)
        
        # Sends login request
        print("Login count: {}".format(state_dict['login_count']))
        state_dict = scraper.login(state_dict)


    return state_dict


def solution_suggestion(state_dict):
    '''
    Sends in solution suggestion for all assignments
    It also tracks the total point gain
    
    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''
    
    # Variables
    state_dict['points_gained'] = 0
    suggestions_path = '../output/5c/scraped_data/suggestion_submitted.json'

    # Read the suggestion file from disk
    file_suggestions = utils.read_json(suggestions_path)

    # Check if the file opened without any problems
    if file_suggestions['status'] == 1:

        state_dict['suggestions'] = file_suggestions['data']
    
    # Else; use empty dictionary
    else:

        state_dict['suggestions'] = {}

    # We need an assignment id
    if state_dict['solution_file'] == {}:

        logging.info("No solution file, activte scrape_mode!")
        raise 'NO_SOLUTION_FILE'


    for k,x in enumerate(state_dict['solution_file']):
        
        submit_suggestion = 0
        
        # Check if we actually need to submit a suggestion
        if 'solution_suggestion_submitted' not in state_dict['solution_file'][x]:
            
            submit_suggestion = 1
        
        elif state_dict['solution_file'][x]['solution_suggestion_submitted'] == 0:
            
            submit_suggestion = 1
        
        # Submit the pixel
        if submit_suggestion == 1:
             
            # Sets the assignemnt_id
            state_dict['assignment_id'] = x

            # Generate a random 1x1 pixel
            state_dict['suggestion'] = random_generators.generate_picture()

            # Submit the pixel
            state_dict = scraper.submit_suggestion(state_dict)
            
            # Verbose point gain
            logging.info("Total point gain: {} | {}/{}".format(
                    state_dict['points_gained'], 
                    k, (len(state_dict['solution_file']) - 1) ))
    
    # Saves the suggestion to file    
    utils.write_json(state_dict['suggestions'], suggestions_path, 
                     "Assignment suggestion written to disk")


    return state_dict


def generate_index(state_dict):
    '''
    Genrates index
    '''

    html_gen.generate_index()
    logging.info("Index generated")

    return state_dict