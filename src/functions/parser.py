# -*- coding: utf-8 -*-
'''
Parses all data
'''

import json
import logging

import utils
from functions import program_state
from functions import html_gen

# Logger init
logger = logging.getLogger(__name__)


def save_assignments(state_dict):
    '''
    Saves the assignments as a JSON file
    
    Attributes:
        * state_dict (dict): Dictionary with the programs current state    
    '''
    
    file_list = []
    file_dict = {}
    chapter_file_path = '../output/5c/scraped_data/assignments.json'
    list_file_path = '../output/5c/scraped_data/assignments_list.json'
    
    # Goes through the chapters
    for k, x in enumerate(state_dict['chapters']):
        
        selected_chapter = state_dict['chapters'][x]['subpart']
        file_dict[x] = selected_chapter
        
        for kx, ky in enumerate(selected_chapter):

            file_list.append(selected_chapter[kx]['assignment_id'])
    
    # Writes all assignments to file
    utils.write_json(file_dict, chapter_file_path, 
                     "Assignments written to file")

    # Writes only the assignmentIds to file
    utils.write_json(file_list, list_file_path, 
                     "AssignmentIds written to file")


def check_for_errors(state_dict):
    '''
    Checks for errors

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''       
    solution = state_dict['solution']
    state_dict['error'] = 0

    try:
        
        # Check for response message
        if 'message' in solution:
    
            state_dict['error'] = 1
            
            # Check that the Token is valid
            if solution['message'] == 'Token Signature could not be verified.':
    
                # Ask for the token
                state_dict['token_valid'] = 0
                logger.info("Token was invalid")
                
            # Check if the token has expired
            if solution['message'] == 'Token has expired':

                # Ask for the token
                state_dict['token_valid'] = 0
                logger.info("Token have expired")
                
                
            # Too many attempts    
            if solution['message'] == "Too Many Attempts.":
            
                logger.warning("SLOW DOWN! {} ".format(solution['assignment_id']))
                
                return state_dict
            
            # Invalid assignment Ids
            if solution['message'] == "Server Error":
            
                logger.warning("Server Error.\
                 No Assignment Id?: {}".format(solution['assignment_id']))

    
        # Check if the solution contains any error
        if 'error' in solution:
            
            logging.debug("Solution error: {}".format(solution['error']))
            
            # Invalid assignment ID
            if solution['error'] == "Tekniskt fel":
                
                logging.info("Invalid assignment id")
                state_dict['assignment_id_valid'] = 0
                
                return state_dict
            
        # Ask for token if it is invalid
        if state_dict['token_valid'] == 0:

            state_dict = utils.ask_for_token(state_dict)
            
            return state_dict
        
    except Exception as e:

        logging.info("Processing solution error: {}".format(e))
        state_dict['error'] = 1
        
        # TODO: FIX INVALID ASSIGNMENTS
        '''
        state_dict['assignment_id'] =  state_dict['solution']
        
    
        logging.info("Trying to download again...")
        time.sleep(3)
        # Gets the solutions
        state_dict = scraper.get_solution(state_dict)
        
        if 'error' in state_dict['solution']:
            
            return state_dict

            # Process the solution
            # state_dict = process_solution(state_dict)
        '''
        
    return state_dict    


def process_solution(state_dict):
    '''
    Processes the solution

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''   
    
    logger.debug("Process_soloution")
    state_dict = check_for_errors(state_dict)

    
    logger.debug("Solution {} did not contain any erros".format(
                                                state_dict['assignment_id']))
    state_dict['assignment_id_valid'] = 0

    #if state_dict['exists'] == 0:

        # TODO: Hmm... Uncessery function??
        # Write to disk
    program_state.write_solution(state_dict)

    html_gen.solution_to_html(state_dict)

    return state_dict


def create_task_id_list(state_dict):
    '''
    Creates a dictionary with corresponding assingment_id for each task_id

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''   
    task_id_translation = {}
    file_path = '../output/5c/scraped_data/task_id_list.json'

    for k,x in enumerate(state_dict['solution_file']):

        assignment_id = x

        if 'solution' in state_dict['solution_file'][x]:

            if state_dict['solution_file'][x]['solution'] != []:

                task_id = state_dict['solution_file'][x]['solution'][0]['task']
                task_id_translation[task_id] = assignment_id

    # Write to disk
    utils.write_json(task_id_translation, file_path, "Task_Id list written to file")
    
    # Save in state_dict
    state_dict['task_id_translation'] = task_id_translation
    

    return state_dict


