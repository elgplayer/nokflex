import logging
import random
import base64
from io import BytesIO

from PIL import Image

logger = logging.getLogger(__name__)

def generate_picture():
    '''
    Generates a random 1x1 pixel and converts the picture to base 64
    
    return: base64 encoded picture (str)
    '''
    
    # Generates  a random 1x1 pixel
    img = Image.new('RGB', (1, 1), color = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)))
    buffer = BytesIO()
    img.save(buffer, format='PNG')
    myimage = buffer.getvalue()
    
    # Converts the pixel to Base64
    picture_base64 = 'data:image/jpeg;base64,' +  base64.b64encode(myimage).decode('utf-8')
    logger.debug("1x1 Pixel generated as Base64")
    
    
    return picture_base64