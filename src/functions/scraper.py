# -*- coding: utf-8 -*-
'''
Contains all functions that scrapes the data
'''

import json
import logging

import requests

from functions import program_state
from functions import parser

logger = logging.getLogger(__name__)


def get_hierarchy(state_dict):
    '''
    Downloads a course hierarchy
    
    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''

    url = "https://nokflex-api.nok.se/api/v2/content/3340"
    querystring = {"courseId":"3340"}
    headers = {
        'sec-fetch-mode': "cors",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'authorization': state_dict['token'],
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'accept': "*/*",
        'cache-control': "no-cache"
        }
    
    response = requests.request("GET", url, headers=headers, 
                                params=querystring).text
    response_json = json.loads(response, encoding='utf-8')
    
    logging.debug("Hierarchy downloaded")
    
    state_dict['hierarchy'] = response_json
    

    return state_dict


def get_hierarchy_assignments(state_dict):
    '''
    Downloads all assignments given a chapter

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''
    
    chapter_id = state_dict['chapter_id']
    
    url = "https://nokflex-api.nok.se/api/v2/assignment/subpart"
    querystring = {"courseId":"3340", "hierarchyId": chapter_id}
    
    headers = {
        'sec-fetch-mode': "cors",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'authorization': state_dict['token'],
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'accept': "*/*",
        'cache-control': "no-cache"
        }
    
    response = requests.request("GET", url, headers=headers, 
                                params=querystring).text
    response_json = json.loads(response, encoding='utf-8')
    logging.debug("Chapter {} downloaded".format(chapter_id))

    state_dict['chapters'][chapter_id] = response_json
    
    
    return state_dict
   

def download_assignments(state_dict):
    '''
    Downloads all assignments for a course
    
    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict    
    '''
    
    # Downloads the hierarchy
    state_dict = get_hierarchy(state_dict)
    
    # Selects the chapters
    chapters = state_dict['hierarchy']['content']['chapters']
    
    # Goes through the chapters
    for chapter in chapters:
        
        # Inserts the chapter ID
        chapter_id = chapter['hierarchyID']
        state_dict['chapter_id'] = chapter_id
        
        # Downloasd all of the hierarchy_assignments
        state_dict = get_hierarchy_assignments(state_dict)
    
    # Save assignments
    parser.save_assignments(state_dict)

    
    return state_dict


def get_solution(state_dict):
    '''
    Gets the soloution from an assignment

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: Soloution as JSON
    '''

    # Check if the ID already is downloaded
    state_dict = program_state.check_for_solution(state_dict)

    # Check if the assignment is already downloaded
    if state_dict['status']['downloaded'] == 1:

        return state_dict
    
    logging.debug("File not downloaded")

    url = "https://nokflex-api.nok.se/api/v2/assignment/solution/{}".format(
                                                state_dict['assignment_id'])
    querystring = {"courseId":"3340"}
    headers = {
        'sec-fetch-mode': "cors",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'authorization': state_dict['token'],
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'accept': "*/*",
        'cache-control': "no-cache",
        }

    # Gets the data
    response = requests.request("GET", url, headers=headers, 
                                params=querystring).text
    # Parses the data as json
    response_json = json.loads(response, encoding='utf-8')

    # Adds to the the state_dict
    state_dict['solution'] = response_json


    return state_dict
    
    
def login(state_dict):
    '''
    Logs in to nokflex

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''
    token = state_dict['token']
    token = token.replace('Bearer ', '')


    url = "https://nokflex-api.nok.se/api/v2/auth/login/"

    payload = str({"access_token":token, "serviceId":"1964"})
    payload = payload.replace("'", '"')
    headers = {
        'sec-fetch-mode': "cors",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'content-type': "application/json;charset=UTF-8",
        'accept': "*/*",
        'cache-control': "no-cache"
        }

    response = requests.request("POST", url, data=payload, 
                                headers=headers).text
    response_json = json.loads(response, encoding='utf-8')
    state_dict['solution'] = response_json
    
    # Check if the response is valid
    if response_json['validated'] == True:

        # Sets the login count in the state_dict
        login_count = response_json['user']['timesSignedIn']
        state_dict['login_count'] = login_count
        
    else: 
        
        # Check for errors
        state_dict = parser.check_for_errors(state_dict)


    return state_dict


def submit_suggestion(state_dict):
    '''
    Submits a sollution sugestion

    Attributes:
        * state_dict (dict): Dictionary with the programs current state

    return: state_dict
    '''

    url = "https://nokflex-api.nok.se/api/v2/user/solution"

    querystring = {"courseId":"3340"}

    payload = str({"assignmentId": state_dict['assignment_id'], 
                  "solution": state_dict['suggestion'], "type": "picture"})
    payload = payload.replace("'", '"')
    headers = {
        'sec-fetch-mode': "cors",
        'origin': "https://nokflex.nok.se",
        'authorization': state_dict['token'],
        'content-type': "application/json;charset=UTF-8",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'x-requested-with': "XMLHttpRequest",
        'dnt': "1",
        'accept': "*/*",
        'cache-control': "no-cache"
        }


    response = requests.request("POST", url, data=payload, 
                                headers=headers, params=querystring).text
    response_json = json.loads(response, encoding='utf-8')
    state_dict['solution'] = response_json
    
    # Check for errors
    state_dict = parser.check_for_errors(state_dict)
    
    if state_dict['error'] == 1:
        
        logging.debug("Error... Trying again")
        state_dict = submit_suggestion(state_dict)
    
    # Check if the suggestion is valid
    if state_dict['solution']['success'] == "true":
        
        selected_assignment = state_dict['solution_file'][state_dict['assignment_id']]
        selected_assignment['solution_suggestion_submitted'] = 1
        
        # Check for point gain
        if state_dict['solution']['pointsGiven'] > 0:

            # Add the point gain
            state_dict['points_gained'] += state_dict['solution']['pointsGiven']
            
            # Add the response of the suggestion to the state_dict
            selected_assignment = state_dict['solution']['solution']
        
    else:
        
        logging.info("Something went wrong: {}".format(state_dict['solution']))
    
    
    return state_dict
        