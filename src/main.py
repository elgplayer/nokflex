# -*- coding: utf-8 -*-
'''
NOKFLEX.se scraper
'''

import logging

import utils
from functions import program_state
from functions import parser

# Create logger
logger = utils.create_logger()
# Logger init
logger = logging.getLogger(__name__)
# Start the logging
logger.info('Started')

program_mode = 'interactive_mode'

# Init the game
state_dict = program_state.init()
state_dict['mode'] = program_mode

logging.info('Program mode: {}'.format(program_mode))

# Interactive mode
if state_dict['mode'] == 'interactive_mode':

    state_dict = program_state.interactive_mode(state_dict)

# Scrapes all assignments
elif state_dict['mode'] == 'scrape_mode':

    state_dict = program_state.scrape_mode(state_dict)   

# Spams login
elif state_dict['mode'] == 'login_spammer':

    state_dict = program_state.login_mode(state_dict)

# Solution suggestion
elif state_dict['mode'] == 'solution_suggestion':

	state_dict = program_state.solution_suggestion(state_dict)

# Generate Index
elif state_dict['mode'] == 'generate_index':

    state_dict = program_state.generate_index(state_dict)
