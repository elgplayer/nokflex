# -*- coding: utf-8 -*-
import os
import json
import pickle
import logging
import sys

# Logger init
logger = logging.getLogger(__name__)


def points_left(status):
    '''
    Goes through the status of the assignments and prints out how many points can be gained

    '''

    nr_of_assignments_left = 0
    potenial_point_gain = 0

    for k,x in enumerate(status):

        try:

            points = status[str(x)]['pointsExtra']

            if points != 0:

                nr_of_assignments_left += 1
                potenial_point_gain += points

        except:

            pass

    logging.info("{} points can be gained with {} unfinished assignments".format(potenial_point_gain, nr_of_assignments_left))
