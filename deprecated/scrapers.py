# -*- coding: utf-8 -*-
import os
import json
import pickle
import logging
import sys

import requests


# Logger init
logger = logging.getLogger(__name__)


def response_codes(response):
    '''
    Takes a respone object and loggs if the responses are bad

    response: response object from request 
    '''

    if response.status_code == 403:

        logging.warning('Unauthorized : {}'.format(response.status_code))    

    if response.status_code == 404:

        logging.warning('Not found: {}'.format(response.status_code))


def assignments_list(url, path, auth):
    '''
    Scrapes all assignments given a url for www.nokflex.se
    Pickles the assignments for redundency
    
    url: Main url to scrape from (str)
    file_path_assignments_list: Path to store the assignments id (str)
    file_path_assignments_info: Path to store the info about each assignment (str)
    auth: Auth dictionary (dict)
    
    return: All assignments (list)
    '''

    # Lists to append to
    assignments_hierarchy = []
    assignment_ids = []
    assignments_question = {}
    
    
    
    # Hmm
    token = auth['token']
    cookie = auth['cookie']
    
    # Request to get the main hir
    querystring = {'token': token}
    
    headers = {
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
        'dnt': '1',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7',
        'cookie': cookie,
        'cache-control': 'no-cache',
    }
    
    # Sends the actual GET request
    response = requests.request('GET', url, headers=headers, params=querystring)
    
    # Loggs if the response is bad
    response_codes(response)
    logging.debug('Response: Status code: %d', response.status_code)


    # Loads the data as json
    try:

        data = response.json()

    except:

        logging.critical('Could not JSON parse essential file, exiting...')
        logging.critical('Did you use NOKFLEX?')

        # Exits program
        sys.exit(1)      
    

    # @FIX Should be a WHILE loop
    # Goes through the data, it's extremly nested so multiple enumeration is needed
    for k, x in enumerate(data['chapters']):
        
        try:
            
            for kx, ky in enumerate(data['chapters'][k]):
                
                for kx2, ky2 in enumerate(data['chapters'][k]['parts'][kx]):
                    
                    try:
                        
                        assignments_hierarchy.append(data['chapters'][k]['parts'][kx]['subParts'][kx2]['hierarchyID'])
                    
                    except:
                        
                        pass
        
        except:
            
            pass
        
        
    # Goes through the main_hir list
    for k, x in enumerate(assignments_hierarchy):
        
        hierarchy_id = str(assignments_hierarchy[k])
        
        # Request to get all the assigments Ids
        querystring = {'token': token, 'hierarchyId': hierarchy_id}
        
        headers = {
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
            'dnt': '1',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7',
            'cookie': cookie,
            'cache-control': 'no-cache',
            }
        
        
        url = 'https://nokflex.nok.se/api/overview/assignments/' # Url to get the Assigments
        
        # Sends the actual GET request
        response = requests.request('GET', url, headers=headers, params=querystring)
        
        # Loads the data as JSON
        json_response = response.json()
        
        # Goes through the JSON response
        for kx, ky in enumerate(json_response):

            # Only assignment ID
            assignment_id = str(json_response[kx]['assignmentID'])
            
            # Appends the response to a list
            assignment_ids.append(assignment_id)
           
            # Detailed info
            assignments_question[assignment_id] = json_response[kx]
            

         
    # Saves all the assigments into a pickle file
    file_path = "{}/assignments_list.pkl".format(path)
    pickle.dump(assignment_ids, open(file_path, 'wb'))
    
    # Saves all the questions into a pickle file
    file_path_questions = "{}/assignments_info.pkl".format(path)
    pickle.dump(assignments_question, open(file_path_questions, 'wb'))

    # Fancy printing
    logging.info('Assignments pickled')
    

    return assignment_ids


def assignments_solution(assignments, file_path, auth):
    '''
    Scrapes the soloution for all assignments
    
    assignments: Assignments id (list)
    file_path: Where to store the file (str)
    auth: Auth dictionary (dict)
    
    return: All assignments soloutions (dictionary)
    '''

    # Hmm
    token = auth['token']
    cookie = auth['cookie']
    
    assignment_solution = {}
    length = len(assignments)
        
    # Goes through the main_hir list
    for k, x in enumerate(assignments):
        
        assignment_id = str(assignments[k])
        logging.info(("Assignment: {}| {}/{}").format(assignment_id, (k+1), length))
        

        # Solution API
        url = 'https://nokflex.nok.se/api/assignment/solution'
        
        querystring = {'assignmentId': assignment_id, 'token': token}
        
        headers = {
            'dnt': '1',
            'x-csrf-token': 'qNFkjq62GbA0qNPSpOpo1mUy9IfnnAbfkunyHXv3',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'x-requested-with': 'XMLHttpRequest',
            'request-id': '|YfMj+.kayci',
            'request-context': 'appId=cid-v1:c4046dde-542b-4e53-9368-e1a08de803a5',
            'referer': 'https://nokflex.nok.se/innehall',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7,nb;q=0.6',
            'cookie': cookie,
            'cache-control': 'no-cache',
            }
        
        response = requests.request('GET', url, headers=headers, params=querystring)
                
        try:
            
            # Loads the data as JSON
            json_response = response.json()
            
            assignment_solution[assignment_id] = json_response
            
        except:
            
            logging.warning('JSON Parse failed. ID: {}'.format(assignment_id))
        
            pass
        
        
         
    # Saves all the assigments into a pickle file
    pickle.dump(assignment_solution, open(file_path, 'wb'))

    # Fancy printing
    logging.info('Assignments SOLUTION pickled')
    

    return assignment_solution


def assignment_status(assignments, file_path, auth):
    '''
    Scrapes the status for each assignments
    
    assignments: Assignments id (list)
    file_path: Where to store the file (str)
    auth: Auth dictionary (dict)
    
    return: All assignments soloutions (dictionary)

    '''

    token = auth['token']
    cookie = auth['cookie']
    
    assignment_status = {}
    file_name = "{}/assignment_status.pkl".format(file_path)

    length = len(assignments)
        
    # Goes through the main_hir list
    for k, x in enumerate(assignments):

        assignment_id = str(assignments[k])
        logging.info(("Assignment: {}| {}/{}").format(assignment_id, (k+1), length))
        
        # Solution API
        url = 'https://nokflex.nok.se/api/assignment/{}/load'.format(assignment_id)
        
        querystring = {'token': token}
        
        headers = {
            'dnt': '1',
            'x-csrf-token': 'qNFkjq62GbA0qNPSpOpo1mUy9IfnnAbfkunyHXv3',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'x-requested-with': 'XMLHttpRequest',
            'request-id': '|YfMj+.kayci',
            'request-context': 'appId=cid-v1:c4046dde-542b-4e53-9368-e1a08de803a5',
            'referer': 'https://nokflex.nok.se/innehall',
            'accept-encoding': 'gzip, deflate, br',
            'cookie': cookie,
            'cache-control': 'no-cache',
            }
        
        response = requests.request('GET', url, headers=headers, params=querystring)
                
        try:
            
            # Loads the data as JSON
            json_response = response.json()
            
            assignment_status[assignment_id] = json_response
            
        except:
            
            logging.warning('Could not parse status for assignment id: {}'.format(assignment_id))
        
            pass
        
        
         
    # Saves all the assigments into a pickle file
    pickle.dump(assignment_status, open(file_path, 'wb'))

    # Fancy printing
    logging.info('Assignments SOLUTION pickled')

    
    return assignment_status