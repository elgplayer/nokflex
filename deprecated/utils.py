# -*- coding: utf-8 -*-
'''
Created on Tue Sep 11 22:30:35 2018

@author: carlelg
'''


import logging
import sys
import os
import random
import base64
from io import BytesIO
import pickle

#from PIL import Image
import requests

from scrapers import assignments_list
from scrapers import assignments_solution
from scrapers import assignment_status

# Logger init
logger = logging.getLogger(__name__)


def create_logger():
    '''
    Creates a logger to log the script
    
    returns: logger object
    '''
    
    # Create logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    
    # Create file handler and set level to debug
    fh = logging.FileHandler('../log/results.log', mode = 'w')
    fh.setLevel(logging.DEBUG)
    
    # Create console handler with a higher log level
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.INFO)
    
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)8s --- %(message)s ' +
                                  '(%(filename)s:%(lineno)s)',datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # Add the handlers to the logger
    root_logger.addHandler(fh)
    root_logger.addHandler(ch)
    
    return logger



def login():
    '''
    *Authentication*

    Reads the two files, 'token.txt' and 'cookie.txt'
    Adds the respective values to a dictionary for authentication and returns it
    
    return: Dictionary with the cookie and the token (dict)
    '''

    # Loads token
    with open('../deb/token.txt', 'r') as read_file:
        token = read_file.read()
    
    # Loads cookies
    with open('../deb/cookie.txt', 'r') as read_file:
        cookie = read_file.read()
          
    auth = {}
    auth['token'] = token
    auth['cookie'] = cookie
    
    return auth



def check_file(path, file_path):
    '''
    Checks if given file exists
    Tries to create diretory if the file doesent exists

    path: Folder path (str)
    file_path: File path (str)
    
    return: If the file exists, returns 1 else returns 0
    '''
    
    if os.path.exists(file_path):
        
        return 1
    
    else:
        
        # Creates directory recursevly
        if not os.path.exists(path):
            
            logging.info('Creating directory: {}'.format(path))
            
            os.makedirs(path, exist_ok = True)
            
        return 0    
<<<<<<< HEAD


def assignment_loader(path, url, auth):
    '''
    Loads the assignment list, either from scraping or from local file
    
    path: Path (str)
    url: Url to scrape from (str)
    auth: Dictionary with cookie and token (dict)
    
    return: Assignments id (list)
    '''
    
    # Filepath
    file_path_assignments_list = '{}/assignments_list.pkl'.format(path) # Only assignment IDs
    file_path_assignments_info = '{}/assignments_info.pkl'.format(path) # All info
    
    print(file_path_assignments_list)

    # Downloads new assignments
    # We only need to check if one exists as they are created at the same time
    if check_file(path, file_path_assignments_list) == 0:
        
        # Calls the scraper function
        logging.info('Scraping the assignments')
        
        assignments = assignments_id(url, file_path_assignments_list, file_path_assignments_info, auth)

=======
>>>>>>> 15674103a73e399d54775a67c17932ca8ba0bdb3
    

def loader(type_of_assignment, path, auth, assignments=None, url=None):
    '''
    Scrapes or loads information

    
    type_of_assignment: What function to call (str)
    assignments: Assignments id (list)
    path: Path (str)
    auth: Dictionary with cookie and token (dict)
    
    return: Depends on what type of assignemnt, most likely (dict) or (list)
    '''
    
    # Filepath
    file_path = '{}/{}.pkl'.format(path, type_of_assignment)
    logging.debug("File path: {}".format(file_path))
    
    # Downloads new assignments
    if check_file(path, file_path) == 0:

        
        # Calls the scraper function
        logging.info('Scraping assignments {}'.format(type_of_assignment))

        # Assignment info
        if type_of_assignment == 'assignments_list':

            data = assignments_list(url, path, auth)
        

        # Solution
        if type_of_assignment == 'solution':
        
           data = assignments_solution(assignments, file_path, auth)


        # Assignment status
        if type_of_assignment == 'assignment_status':

            data = assignment_status(assignments, file_path ,auth)
    

    else:
        
        # Loads the pickle file
        logging.info('{} loaded from file'.format(type_of_assignment))
        
        data = pickle.load(open(file_path, 'rb'))


    return data


def generate_picture():
    '''
    Generates a random 1x1 pixel and converts the picture to base 64
    
    return: base64 encoded picture (str)
    '''
    
    # Generates  a random 1x1 pixel
    img = Image.new('RGB', (1, 1), color = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)))
    buffer = BytesIO()
    img.save(buffer, format='JPEG')
    myimage = buffer.getvalue()
    
    # Converts the pixel to Base64
    picture_base64 = 'data:image/jpeg;base64,' +  base64.b64encode(myimage).decode('utf-8')
    
    
    return picture_base64


def check_answer(assignment_id, num, answer, auth):
    '''
    Checks if the answer is correct
    
    assignment_id: Id for the given assignment (str)
    num: Which question on the assignment, e.g: a, b, c (str)
    answer: Answer (str)
    auth: Auth dictionary (dict)
    
    return: 1 (int) if correct else 0 (int)
    '''

    token = auth['token']
    cookie = auth['cookie']

    url = 'https://nokflex.nok.se/api/assignment/verifySingle/{}/{}'.format(assignment_id, num)
    querystring = {'userAnswer': answer}
    
    headers = {
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
        'dnt': '1',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7',
        'cookie': cookie,
        'cache-control': 'no-cache',
    }
    
    verify_answer = requests.request('GET', url, headers=headers, params=querystring)
    verify_answer = verify_answer.json()
    
    return verify_answer['correct']

