import os
import json
import pickle
import sys
import logging

import requests

from utils import create_logger
from utils import login
from utils import loader

from analyser import points_left


# Authentication
auth = login()
token = auth['token']
cookie = auth['cookie']

#### USER INPUT ############

url = 'https://nokflex.nok.se/api/content/3404' # Matte 4C

folder_name = 'uppgifter_4c'
output_path = '../output'
path = '{}/{}'.format(output_path, folder_name)

############################

# Create logger
logger = create_logger()

# Get the loggers name
logger = logging.getLogger(__name__)

# Start the logging
logging.info('Started')

# Verbose
logging.debug('Parameters:') 
logging.debug('File name = {}'.format(folder_name)) 
logging.debug('Path = {}'.format(path)) 
logging.debug('URL = {}'.format(url)) 


# Loads the assignments
assignments = loader('assignments_list', path, auth, None, url)

# Gets the SOLUTION from the assignments
# ***WARNING*** - THIS WILL REMOVE ALL POSSIBLE POINTS TO GAIN - ***WARNING*** 
#solution = loader('solution', path, auth, assignments)

# Gets the status from the assignments
status = loader('assignment_status', path, auth, assignments)

points_left(status)

# Finished
logging.info('Finished')